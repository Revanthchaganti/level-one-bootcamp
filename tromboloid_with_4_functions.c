//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input()
{
float a;
printf("Enter the height, depth and breadth: ");
scanf("%f", &a);
return a;
}
float volume(float a, float b, float c)
{
float volume;
volume = ((a*b*c) + (b/c)) * 1/3;
return volume;
}
float output(float c)
{
printf("Volume of tromboloid is %f \n", c);
}
int main()
{
float h,d,b,v;
h= input();
d= input();
b= input();
printf("The dimensions of the tromboloid are height-%f, depth-%f and breadth-%f \n", h, d, b);
v= volume(h,d,b);
output(v);
return 0;
}